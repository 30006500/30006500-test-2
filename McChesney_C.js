/*Campbell McChesney
ID: 30006500*/


//Variables below are defined and a prompt asks for user input
let VanID = prompt(`EZ Van Hire: Enter Van Details\n VanID:`)
let Capacity = prompt(`EZ Van Hire: Enter Van Details\n Load Capacity (m3):`)
let License = prompt(`EZ Van Hire: Enter Van Details\n License Type:`)
let CostPerDay = prompt(`EZ Van Hire: Enter Van Details\n Hire Cost Per Day ($):`)
let Insurance = prompt(`EZ Van Hire: Enter Van Details\n Insurance Per Day ($):`)
let total = 0

class Van //A class called Van is created
    {
        constructor(VanID, Capacity, License, CostPerDay, Insurance) //Variables obtained from the newly constructed class instance Van1
            {
                //The constructor assigns each variable to a property to be used as getter and setter names
                this.aVanID  = VanID 
                this.aCapacity = Capacity
                this.aLicense = License
                this.aCostPerDay = CostPerDay
                this.aInsurance = Insurance
            }
    
            //Each getter is a special funtion that gets the variable from the setter and returns it to the main program, in the first case _VanID
        get aVanID()
            {
                return this._VanID
            }
            //Each setter takes the variable from the constructor and sets the property name, in the first case _VanID
        set aVanID(value)
            {
                this._VanID = value
            }

        get aCapacity()
            {
                return this._Capacity
            }

        set aCapacity(value)
            {
                this._Capacity = value
            }
        
        get aLicense()
            {
                return this._License
            }
        
        set aLicense(value)
            {
                this._License = value
            }

        get aCostPerDay()
            {
                return this._CostPerDay
            }
        
        set aCostPerDay(value)
            {
                this._CostPerDay = CostPerDay
            }

        get aInsurance()
            {
                return this._Insurance
            }

        set aInsurance(value)
            {
                this._Insurance = Insurance
            }

        
    }

let Van1 = new Van (VanID, Capacity, License, CostPerDay, Insurance) //This creates a new instance of the Van Class called Van1 and gets the variables from the user prompts

function totalCost() //Creates a function to calculate the total cost
    {
        return total = Number(Van1.aCostPerDay) + Number(Van1.aInsurance) //This Calculates the sum of cost per day and insurance from the Van1 Class
    }
//All of the code below provides output information taken from the getters in the Van class
console.log(`EZ Van Hire: Van Details`)
console.log(`------------------------`)
console.log(`Van ID: ${Van1.aVanID}`)
console.log(`Load Capacity (m3): ${Van1.aCapacity}`)
console.log(`License Type: ${Van1.aLicense}`)
console.log(`Hire Cost Per Day: $${Van1.aCostPerDay}`)
console.log(`Insurance Per Day: $${Van1.aInsurance}`)
console.log(`Total Hire Cost: $${totalCost()}`) //calls the function to return the total cost